/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package hlanz.programacion.criptografia.cesar;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aliherrerasz
 */
public class ImplementacionCesarTest {
    
    public ImplementacionCesarTest() {
    }

    @Test
    public void testSomeMethod() {
        //ImplementacionCesar ic = new ImplementacionCesar();
        //assertEquals('B',ic.descifrar("A", "1"));/*
        
        assertEquals('B',ImplementacionCesar.getLetraDesplazada('A', 1));
        assertEquals('K',ImplementacionCesar.getLetraDesplazada('H', 3));
        assertEquals('A',ImplementacionCesar.getLetraDesplazada('Z', 1));
        assertEquals('Z',ImplementacionCesar.getLetraDesplazada('A', -1));
        assertEquals('H',ImplementacionCesar.getLetraDesplazada('K', -3));
        assertEquals('D',ImplementacionCesar.getLetraDesplazada('V', 8));
        assertEquals('V',ImplementacionCesar.getLetraDesplazada('B', -8));
       /* assertEquals("KROD", ImplementacionCesar.desplazarPalabra("HOLA", 3));          
        assertEquals("HOLA", ImplementacionCesar.desplazarPalabra("KROD", -3));          
        assertEquals("IPMB", ImplementacionCesar.desplazarPalabra("HOLA", 1));          
        assertEquals("HOLA", ImplementacionCesar.desplazarPalabra("IPMB", -1));          
        assertEquals("ZAPATO", ImplementacionCesar.desplazarPalabra("BCRCVQ", -3));          
        assertEquals("BCRCVQ", ImplementacionCesar.desplazarPalabra("ZAPATO", 2));   */     
    }
    @Test
    public void test2() {
        
    }
    @Test
    public void test3() {
        
    }
    @Test
    public void test4() {
        
    }
    @Test
    public void test5() {
        
    }
    @Test
    public void test6() {
        
    }
    
    
}
