/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package hlanz.programacion.criptografia.general;

import java.awt.List;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Usuario
 */
public class CriptografiaAbstractFactoryTest {
    
    public CriptografiaAbstractFactoryTest() {
    }

    @Test
    /*Llama al método getFactory dos veces, con las constantes
AlgoritmoCifrado.CESAR y AlgoritmoCifrado.ROT13 y comprueba que en ambos
casos se obtienen objetos no nulos (se recomienda usar un bucle, para así no
tener que tocar el test si se añaden en el futuro nuevos algoritmos)*/
    public void test1() {
        for (int i =0; i<2; i++){
        assertNotNull (CriptografiaAbstractFactory.getFactory(AlgoritmoCifrado.CESAR));
        assertNotNull (CriptografiaAbstractFactory.getFactory(AlgoritmoCifrado.ROT13));
        } 
    }
    
    
    @Test
    /*Llama al método getFactory con la constante AlgoritmoCifrado.CESAR y
comprueba con instanceof que el objeto recibido es de tipo CesarFactory */
    public void test2(){
       
    }
    
}
