/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package hlanz.programacion.criptoanalisis.util;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Usuario
 */
public class AnalizadorFraseTest {
    
    public AnalizadorFraseTest() {
    }

    @Test
    public void test1() {
        String frase = "HOY ES LUNES";
        AnalizadorFrase analizador = new AnalizadorFrase(frase);
        assertEquals(3, analizador.getNumeroPalabras());

        //El método getPalabras nos devuelve una lista de 3 elementos
        List<String> lista = analizador.getPalabras();
        assertEquals(3, lista.size());

        //Saca los 3 elementos de la lista obtenida en el punto anterior y 
        //comprueba que
        //son las palabras “HOY”, “ES” y “LUNES”
        assertEquals("HOY", lista.get(0));
        assertEquals("ES", lista.get(1));
        assertEquals("LUNES", lista.get(2));
    }

    @Test
    public void test2() {
        /*Repite el test anterior para la frase “YO ESTUDIO EN EL INSTITUTO
POLITECNICO HERMENEGILDO LANZ” (en el último de los puntos guarda las
palabras de la frase del test en un String[] y haz un bucle para hacer los
assertEquals correspondientes con las palabras que salen del List<String>).*/
        String frase2 = "YO ESTUDIO EN EL INSTITUTO POLITECNICO HERMENEGILDO LANZ";
        AnalizadorFrase analizador2 = new AnalizadorFrase(frase2);
        List<String> lista2= analizador2.getPalabras();

        assertEquals(8, analizador2.getNumeroPalabras());
        
        assertEquals(8,lista2.size());
        
      
        for (int i=0; i<lista2.size();i++){
            String[] palabras= new String [lista2.size()];
            palabras [i]= lista2.get(i);
            
            assertEquals(lista2,lista2.get(i));
        }
        
        
    }

}
