/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptoanalisis.general;

/**
 *
 * @author Usuario
 */
public interface Hackeador {
    public HackResult descifrar(String texto);
}
