package hlanz.programacion.criptoanalisis.general;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Idioma {

    private String nombre;
    private Set<String> palabras;

    public Idioma(String nombre, String archivo) throws IOException {
        this.nombre = nombre;
        palabras = new HashSet<>();
        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                palabras.add(linea.toUpperCase());
            }
        } catch (IOException e) {
            throw e;
        }
    }

    public String getNombre() {
        return nombre;
    }

    public boolean contienePalabra(String palabra) {
        return palabras.contains(palabra.toUpperCase());
    }

    public boolean contieneFrase(String frase, int porcentajeTolerancia) {
        if (porcentajeTolerancia < 0 || porcentajeTolerancia > 100) {
            throw new IllegalArgumentException("El porcentaje de tolerancia debe estar entre 0 y 100");
        }
        String[] palabrasFrase = frase.split("\\s+");
        int numPalabrasFrase = palabrasFrase.length;
        int numTolerancia = Math.round(numPalabrasFrase * porcentajeTolerancia / 100);
        int numPalabrasIdioma = 0;
        for (String palabra : palabrasFrase) {
            if (contienePalabra(palabra)) {
                numPalabrasIdioma++;
            }
        }
        return numPalabrasIdioma > numTolerancia;
    }
}
