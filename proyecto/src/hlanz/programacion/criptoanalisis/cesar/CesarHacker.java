/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptoanalisis.cesar;

import hlanz.programacion.criptoanalisis.general.Hackeador;
import hlanz.programacion.criptoanalisis.general.*;
import hlanz.programacion.criptografia.cesar.CesarFactory;
import hlanz.programacion.criptografia.general.Descifrador;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author usuario
 */
public class CesarHacker implements Hackeador{
    protected  List<Idioma> idiomasPosibles;
    protected int porcentajeTolerancia;
    
    CesarHacker(int porcentajeTolerancia)throws IOException{
      this.porcentajeTolerancia=porcentajeTolerancia; 
        Idioma espanol = new Idioma( "Español", "../spanish.txt");
        Idioma ingles = new Idioma("ingles", "../ingles.txt");
        idiomasPosibles.add(ingles);
        idiomasPosibles.add(espanol);

    
    }
    

    @Override
    public HackResult descifrar(String texto) {
      
      CesarFactory cf = new CesarFactory();
      Descifrador descrifrador = cf.getDescifrador();
      boolean exito=true;
         HackResult hr=null;
        for (int i = 1; i <= 26 && exito; i++) {
            
            descrifrador.descifrar(texto, ""+i);
            
            boolean idiamaIngles =idiomasPosibles.get(0).contieneFrase(texto, porcentajeTolerancia);
            boolean idiamaEspañol =idiomasPosibles.get(1).contieneFrase(texto, porcentajeTolerancia);
            if(idiamaIngles){
                exito=false;
                
                hr = new HackExito(texto,  descrifrador.descifrar(texto, ""+i), ""+i, idiomasPosibles.get(0));
            }else if(idiamaEspañol){
                hr = new HackExito(texto,  descrifrador.descifrar(texto, ""+i), ""+i, idiomasPosibles.get(1));
            }
            
            
        }
      
      
      hr = new HackFracaso("No se ha podido encontarar un porcentage de asociación del "+porcentajeTolerancia+" Con el texto");

 
        return  hr;
      
    }

   
}
