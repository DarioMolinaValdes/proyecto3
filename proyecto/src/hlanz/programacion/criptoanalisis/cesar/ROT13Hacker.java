/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptoanalisis.cesar;

import hlanz.programacion.criptoanalisis.general.*;
import hlanz.programacion.criptografia.cesar.*;
import hlanz.programacion.criptografia.general.*;
import java.io.*;

public class ROT13Hacker extends CesarHacker {

    public ROT13Hacker(int porcentajeTolerancia) throws IOException {
        super(porcentajeTolerancia);
    }

    @Override
    public HackResult descifrar(String texto) {
        ROT13Factory rf = new ROT13Factory();
        Descifrador dr = rf.getDescifrador();
        String frase = dr.descifrar(texto, texto);
        HackResult hr = null;
        for (Idioma i : idiomasPosibles) {
            if (i   .contieneFrase(texto, porcentajeTolerancia)) {
                hr = new HackExito(texto, frase, "13", i);
            } else {
                hr = new HackFracaso("Idioma desdonocido");
            }
        }
        return hr;
    }
}
