/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptoanalisis.cesar;

import hlanz.programacion.criptoanalisis.general.Hackeador;
import hlanz.programacion.criptografia.general.*;
import java.io.IOException;


public class HackeadorFactory {
    public static Hackeador getHackeador(AlgoritmoCifrado algoritmo, int porcentajeTolerancia) throws IOException {
        if (porcentajeTolerancia < 0 || porcentajeTolerancia > 100) {
            throw new IllegalArgumentException("El porcentaje de tolerancia debe estar entre [0,100]");
        }

        if (algoritmo == AlgoritmoCifrado.CESAR) {
            return new CesarHacker(porcentajeTolerancia);
        } else if (algoritmo == AlgoritmoCifrado.ROT13) {
            return (Hackeador) new ROT13Hacker(porcentajeTolerancia);
        }//Esta to mu mal realizado

        throw new IllegalArgumentException("Algoritmo de cifrado no válido");
    }
}
