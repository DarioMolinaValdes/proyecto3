/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptoanalisis.util;

import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author usuario
 */
public class AnalizadorFrase {

    private String[] palabras;

    public AnalizadorFrase(String frase) {

        StringTokenizer palabra = new StringTokenizer(frase, " ");
        boolean repetir = true;
        for (int i = 0; i <= palabras.length && repetir; i++) {
            if (palabra.hasMoreTokens() == true) {
                String[] b = {palabra.nextToken()};
                palabras = b;
            } else {
                repetir = false;
            }
        }
    }

    public int getNumeroPalabras() {
        return palabras.length;
    }

    public List<String> getPalabras(){
        List<String> lista=null;
        for (int i = 0 ; i <= palabras.length; i++){
        lista.add(palabras[i]);
        }
        return lista;
    }
}
