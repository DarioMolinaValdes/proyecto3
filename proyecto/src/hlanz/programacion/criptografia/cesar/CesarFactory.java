/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptografia.cesar;
import hlanz.programacion.criptografia.general.*;

/**
 *
 * @author aliherrerasz
 */
public class CesarFactory implements CriptografiaFactory{
    private ImplementacionCesar cesar;
    
    public CesarFactory() {
        cesar = new ImplementacionCesar();
    }

    @Override
    public Cifrador getCifrador() {
        return (Cifrador) this.cesar;
    }

    @Override
    public Descifrador getDescifrador() {
        return (Descifrador) this.cesar;
    }
}