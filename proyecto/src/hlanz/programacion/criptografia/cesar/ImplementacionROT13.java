/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptografia.cesar;

/**
 *
 * @author aliherrerasz
 */
public class ImplementacionROT13 extends ImplementacionCesar {

    ImplementacionROT13() {
        // No hace nada, según la documentación
        
    }
    @Override
    public String descifrar(String texto, String clave){ 
        return super.descifrar(texto, "13");
    }
    
    @Override
    public String cifrar(String texto, String clave){ 
        return super.cifrar(texto, "13");
    }
}