/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptografia.cesar;

import hlanz.programacion.criptografia.general.Cifrador;
import hlanz.programacion.criptografia.general.CriptografiaFactory;
import hlanz.programacion.criptografia.general.Descifrador;

/**
 *
 * @author aliherrerasz
 */
public class ROT13Factory implements CriptografiaFactory {
    private ImplementacionROT13 rot13;
    
    public ROT13Factory(){
        rot13 = new ImplementacionROT13();
    }

    @Override
    public Cifrador getCifrador() {
        return (Cifrador) rot13;
    }

    @Override
    public Descifrador getDescifrador() {
        return (Descifrador) rot13;
    }
}