
package hlanz.programacion.criptografia.cesar;

import hlanz.programacion.criptografia.general.*;
import hlanz.programacion.criptoanalisis.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author aliherrerasz
 */
 public class ImplementacionCesar implements Cifrador, Descifrador{
    
     
     ImplementacionCesar(){
        // Según la documentación: "no hace nada"
    }
     
    protected static char getLetraDesplazada(char letra, int desplazamiento){
        if(letra>='A' && letra <='Z'){
            letra = (char) (letra + desplazamiento);
            if(letra>'Z'){
                int diferencia = letra -'Z';
                letra = (char) ('A' + (diferencia-1));
            }else if(letra<'A'){
                int diferencia = 'A' - letra;
                letra = (char) ('Z' - (diferencia+1));
            }
        } else {
            throw new IllegalArgumentException("Solo se pueden desplazar letras mayúsculas");
        }
        return letra;
    }
    protected static String desplazarPalabra(String palabra, int desplazamiento){
        String palabraMayuscula = palabra.toUpperCase();
        String letrasDesplazadas = null;
        for(int i = 0; i<palabraMayuscula.length();i++){
            char letraRecorrida = palabraMayuscula.charAt(i);  
            letrasDesplazadas += getLetraDesplazada(letraRecorrida, desplazamiento);
        }
        return letrasDesplazadas;
    }
    protected static String desplazarLetrasFrase(String frase, int desplazamiento){
        /*List<String> palabrasFrase = new ArrayList<>();
        String fraseDesplazada = null;
        StringTokenizer st = new StringTokenizer(frase, " ");
        palabrasFrase.add(st.nextToken()+" ");
        while(st.hasMoreTokens()){
            palabrasFrase.add(st.nextToken()+" ");
        }
        return fraseDesplazada;*/
        AnalizadorFrase af = new AnalizadorFrase(frase);
        List<String> palabras = af.getPalabras();
        String palabraDesplazada = null;
        StringTokenizer st = new StringTokenizer(frase, " ");
        for(int i = 0; i<st.countTokens(); i++){
            String palabra = desplazarPalabra(st.nextToken(), desplazamiento);
            palabraDesplazada += palabra;
        }
        return palabraDesplazada;
    }

    @Override
    public String descifrar(String texto, String clave) {
        int valorClave = Integer.parseInt(clave);
        String descifrado = "frase sin descifrar";
        if(valorClave<0){
            throw new IllegalArgumentException("La clave debe ser un número positivo");
        } else {
            valorClave = valorClave * (-1);
            descifrado = desplazarLetrasFrase(texto, valorClave);
        }
        return descifrado;
    }

    @Override
    public String cifrar(String texto, String clave) {
        int valorClave = Integer.parseInt(clave);
        String descifrado = "frase sin descifrar";
        if(valorClave<0){
            throw new IllegalArgumentException("La clave debe ser un número positivo");
        } else {
            descifrado = desplazarLetrasFrase(texto, valorClave);
        }
        return descifrado;
    }
}