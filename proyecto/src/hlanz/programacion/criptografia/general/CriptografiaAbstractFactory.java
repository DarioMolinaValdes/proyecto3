/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hlanz.programacion.criptografia.general;

import hlanz.programacion.criptografia.cesar.CesarFactory;
import hlanz.programacion.criptografia.cesar.ROT13Factory;

/**
 *
 * @author Usuario
 */
public class CriptografiaAbstractFactory implements CriptografiaFactory {

    public static CriptografiaFactory getFactory(AlgoritmoCifrado algoritmo) {

         CriptografiaFactory resultado = null;
        if (algoritmo.equals(algoritmo.CESAR)) {
            resultado= (CriptografiaFactory) new CesarFactory();

        } else if (algoritmo.equals(algoritmo.ROT13)) {
            resultado= (CriptografiaFactory) new ROT13Factory();
        }
        return resultado;
    }


    @Override
    public Cifrador getCifrador() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Descifrador getDescifrador() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
