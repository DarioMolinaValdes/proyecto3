/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hlanz.programacion.criptografia.general;

/**
 *
 * @author Usuario
 */
public interface CriptografiaFactory {
    public Cifrador getCifrador();
    public Descifrador getDescifrador();
}
